title: gnuplot绘图笔记
date: 2014-04-07 19:56:17
tags: gnuplot
categories: notes
description: gnuplot绘图的个人笔记，逐渐更新。
---

>gnuplot是一套跨平台的数学绘图自由软件。使用交互式接口，可以绘制数学函数图形，也可以从纯文字档读入简单格式的座标资料，绘制统计图表等等。它不是统计软件，也不是数学软件，它纯粹只是一套函数／资料绘图软件。

```bash
plot f(x)	#2D绘图
splot f(x)	#3D绘图
replot	#重新绘图
set xrange [-20:20]	#x轴取值范围，yrange同理
set xlabel 'x'	#x轴标签，ylabel/zlabel
set xtics 10	#x轴每格代表的数值，ytics/ztics
set isosamples 50	#取样率，决定图像的精细程度
set size ratio 1	#图像比例

###以下为3D图像特殊命令
set ticslevel 0	#z轴上移的数值，负数表示下移
set hidden3d	#隐藏被遮挡的部分
set pm3d	#启用着色
set view map	#平面化，可用于模拟衍射图样等
set palette gray	#设定配色为灰色
set palette gamma 3	#修改图像的gamma值
set palette rgbformulae 8,0,0	#自定义配色方案，help rgbformulae查看详细说明

###以下为保存文件相关命令
set terminal jpeg enhanced	#保存为jpeg格式，并开启增强模式（可输入上下标等）
set terminal jpeg size 480,480	#图像文件分辨率
set output 'fig1.jpg'	#文件名
```
