title: 为Debian的bash启用自动补全
date: 2014-05-07 18:20:23
tags:
 - debian
 - bash
 - linux
categories: notes
description: 启用Debian中的bash-completion，使用TAB键自动补全命令。
---
在Ubuntu的终端中使用`TAB`键可以自动补全命令，换到Debian之后发现没有这个功能，相当不适应。之前搜索到的解决方法都是修改`/etc/profile`文件，但是我修改后无法登录图形化界面。今天终于找到一个有效的方法。

首先，确认已经安装了`bash-completion`，这个应该是Debian默认安装了的。

然后修改`/etc/bash.bashrc`文件，找到以下被注释的代码，取消掉注释。
```
# enable bash completion in interactive shells
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
```
注销并重新登录即可。
