title: 静态博客如何实现站内搜索
date: 2014-01-28 15:02:49
tags:
 - hexo
 - tapir
 - swiftype
categories: web
description: 静态博客由于不使用数据库，实现站内搜索相对比较麻烦，这里给出了三种简单的方法：百度、Tapir和swiftype
---

Hexo这种静态博客由于不使用数据库，相对Wordpress来说实现站内搜索没有直接的办法，只能通过其他网站提供的服务来实现。这里简单介绍我曾使用过的方法，以我使用的时间为序，所以个人认为的最好的办法在最后面。

ps.以下操作均以Hexo下 [chenall](https://github.com/chenall/hexo-theme-chenall) 主题为例，其他平台或主题请自行类比。
<!--more-->

##百度高级搜索

利用百度搜索，添加`site:[YOUR_URL]`的方式实现。chenall主题本身自带的搜索也是同样的方式，只不过用的是google，这里改为百度更利于国内用户。在`themes/chenall/layout/_widgeets`目录（或`source/_widgets`）添加文件`baidu.ejs`，代码如下：

```xml baidu.ejs
<form action="http://www.baidu.com/s" method="get" accept-charset="utf-8">
  <div class="input-group">
    <input class="form-control" id="searchbox" type="search" name="q1" results="0" placeholder="<%= __('search') %>">
    <span class="input-group-btn">
      <button class="btn btn-default" type="submit">Go!</button>
    </span>
    <input type="hidden" name="q6" value="<%- config.url.replace(/^https?:\/\//, '') %>">
  </div>
</form>
```

然后在`themes/chenall/_config.yml`（或`source/_chenall.yml`）启用

```yaml
 widgets:
  sidebar: #侧边栏
   - baidu
```

缺点：搜索结果取决于百度的索引，因此并不是真正意义上的站内搜索。

##Tapir API

Tapir利用RSS建立索引，所以首先要使用 [hexo-generator-feed](https://github.com/hexojs/hexo-generator-feed) 生成`atom.xml`，然后到 [Tapir的官方网站](http://tapirgo.com/) 提交你的RSS地址，获取token并记录下来。Tapir会每隔15分钟获取一次RSS并将新文章加入到索引中。利用 [Tapir Search API](http://tapirgo.com/#search_api) 可获取JSON格式的搜索结果。

最简单的使用方法是直接使用官方提供的 [jQuey plugin](https://github.com/TapirGo/jQuery-Plugin) ，添加侧边栏`tapir.ejs`并启用（具体操作同上）

```xml tapir.ejs
<form action="/search/" accept-charset="utf-8">
  <div class="input-group">
    <input class="form-control" id="searchbox" type="text" name="query" placeholder="<%= __('search') %>">
    <span class="input-group-btn">
      <button class="btn btn-default" type="submit">Go!</button>
    </span>
  </div>
</form>
```

然后新建一个名为`search`的page

```md
title: Search Results
comments: false
---
<div id="search_results"></div>
<script src="js/jquery.min.js"></script>
<script src="js/jquery-tapir.min.js"></script>
<script>
$('#search_results').tapir({'token': 'YOUR_TOKEN'});
</script>
```

其中`jquery-tapir.min.js`可从 [这里](https://github.com/TapirGo/jquery-plugin/raw/master/jquery-tapir.min.js) 获取。

缺点：不可指定索引内容，搜索结果的个性化定制难度较大

##swiftype

**强力推荐！**能自动抓取RSS或sitemap，并且支持手动新增页面，以及删除不想被索引的内容。支持自定义排序，支持自动完成，支持搜索统计，支持自定义样式，支持多种搜索结果展示方式…… 几乎完美的站内搜索引擎，支持所有网站。具体参考 [官方文档](https://swiftype.com/documentation) 。

在 [swiftype](https://swiftype.com/) 官网注册登陆后，根据提示添加域名即可自动索引，在INSTALL页面可直接获取代码，下面贴出适合chenall主题使用的代码：

```xml swiftype.ejs
<form>
<div class="input-group">
<input type="text" id="st-search-input" class="form-control" placeholder="<%= __('search') %>" />
    <span class="input-group-btn">
      <button class="btn btn-default" type="submit">GO</button>
    </span>
</div>
</form>
<script type="text/javascript">
var Swiftype = window.Swiftype || {};
  (function() {
    Swiftype.key = 'YOUR_KEY';
    var script = document.createElement('script'); script.type = 'text/javascript'; script.async = true;
    script.src = "//s.swiftypecdn.com/embed.js";
    var entry = document.getElementsByTagName('script')[0];
    entry.parentNode.insertBefore(script, entry);
  }());
</script>
```

