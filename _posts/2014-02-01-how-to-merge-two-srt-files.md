title: 合并两个srt字幕文件
date: 2014-02-01 23:44:19
tags:
 - linux
 - diff
 - coursera
categories: notes
description: 使用linux下的diff命令合并中文和英文字幕为双语字幕
---

在Coursera上课的时候遇到这样的情况：有些课程同时提供英文字幕和中文字幕，但是没有双语字幕，这时就需要将两个字幕合并起来，其实看电影和美剧的时候偶尔会遇到这样的情况（好吧我承认我有必须看双语字幕的强迫症）。我知道linux下的`diff`命令可以用作比较文本文件，但是一直不太懂具体的用法，今天百度查了一下，赶紧记下来以免遗忘。

假设有`zh.srt`和`en.srt`两个文件

	$ diff zh.srt en.srt -D NAME > new.srt

其中`NAME`可自定义，用于生成注释(output merged file with `#ifdef NAME' diffs)

这时的`new.srt`中还有类似`#ifdef NAME`的注释句，在你常用的文本编辑器中写正则去掉即可，也可以利用`cat`命令：

	$ cat new.srt |grep -v '^\#.*' > ok.srt

注：以上仅适用于两个字幕文件的时间轴完全一致的情况
