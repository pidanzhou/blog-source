title: 升级到了 Ubuntu 14.04 (Trusty Tahr)
date: 2014-04-13 16:53:00
tags: 
  - ubuntu
  - linux
categories: notes
description: 记录了安装Ubuntu 14.04之后需要做的几件事。
---

本来是想等到17号正式发布之后再升级的，但偏偏在上周系统出现了一些莫名奇妙的问题，干脆直接重新安装了。（跨版本升级总感觉不可靠。。。）这里稍微记录一下安装完之后我做了些什么吧。

##安装显卡驱动
因为我是N卡的笔记本，安装完之后第一反应是要装`bumblebee`，但是发现自14.04开始已经有更好的原生方案来支持显卡了，那就是`nvidia-prime`。（其实13.10也是支持的，只要驱动版本在319.32以上即可）

原文地址：[USING NVIDIA GRAPHICS DRIVERS WITH INITIAL OPTIMUS SUPPORT IN UBUNTU 13.10 GOT EASIER WITH NVIDIA-PRIME](http://www.webupd8.org/2013/08/using-nvidia-graphics-drivers-with.html)

首先，如果之前有安装过`bumblebee`要先卸载掉。原文作者还提醒，确认没有安装`libvdpau-va-gl1`；

	sudo apt-get purge bumblebee*
	sudo apt-get purge libvdpau-va-gl1

安装N卡驱动和`nvidia-prime`（这里我用的是官方源的最新驱动版本331）；

	sudo apt-get install nvidia-331 nvidia-settings-331 nvidia-prime

最后重启。

<!--more-->

之后，使用`prime-select`命令或者使用`nvidia-settings`可以切换GPU，但是需要注销或重启之后在能生效，这是相比`bumblebee`最大的不足之处。

![nvidia-settings](/images/1404/nvidia-settings.jpg)

如果你嫌每次输入命令切换太麻烦，[这里](http://www.webupd8.org/2014/01/prime-indicator-lets-you-quickly-switch.html)有个叫做`prime-indicator`的软件，可以显示目前使用的GPU并很方便的切换。注意：切换后会重启X Server，切换前请先保存好重要文档。

![prime-indicator](/images/1404/prime-indicator.jpg)
```bash
sudo add-apt-repository ppa:nilarimogard/webupd8
sudo apt-get update
sudo apt-get install prime-indicator
```

##wifi共享（AP）
之前一直用eexpress写的 [ap.bash] 来打开wifi共享给手机使用，依赖包为`hostapd`和`dhcp3-server`。升级14.04之后无法正常启用，发现与新版本的`hostapd`有关，暂时的解决方法是降级到saucy源的版本 [hostapd (1:1.0-3ubuntu2.1)] 。下载后`sudo dpkg -i hostapd_1.0-3ubuntu2.1_amd64.deb`即可。
[ap.bash]:https://github.com/eexpress/eexp-bin/blob/master/ap.bash
[hostapd (1:1.0-3ubuntu2.1)]:http://packages.ubuntu.com/saucy-updates/hostapd

##降级maxima解决绘图出错
新版本的maxima也存在一些问题，体现为绘图出错。暂时未找到问题的原因所在，因此只能先降级使用。saucy源的 [maxima (5.30.0-12)] 以及依赖包 [maxima-share (5.30.0-12)] 。同样地使用`sudo dpkg -i *.deb`安装。
[maxima (5.30.0-12)]:http://packages.ubuntu.com/saucy/maxima
[maxima-share (5.30.0-12)]:http://packages.ubuntu.com/saucy/maxima-share
