title: 终于将博客从Wordpress迁移到Hexo了
date: 2014-01-26 20:21:06
tags:
 - hexo
 - wordpress
categories: web
description: 很早就看上了Hexo这个静态博客框架，今天正式开始使用它。这里简单介绍了Hexo的特点。
---

很早就看上了 [Hexo](http://zespia.tw/hexo) 这个静态博客框架，但是一直没有时间好好去了解。上周终于开始尝试搭建，并且看上一个很棒的主题 [hexo-theme-chenall](https://github.com/chenall/hexo-theme-chenall) ，但这几天却又在忙其他事情，一直没能彻底替换掉原来的Wordpress博客。今天终于完工了！本来是打算利用 [hexo-migrator-wordpress](https://github.com/hexojs/hexo-migrator-wordpress) 这个插件把原来的博文转移过来，其实没这必要，因为根本没有几篇好东西。。。原有Wordpress博客暂时保留在BAE上，网址是 <http://blog2.moyizhou.cn>，说不定哪天会让它彻底挂掉。

之前我在想要不要在这里写一下Hexo的安装和配置的步骤，后来发现 [官方文档](http://zespia.tw/hexo/docs/) 已经写得非常详细了，我再写点什么实属多余。不管怎样，作为第一篇博文，还是应该简单介绍一下Hexo的特点。

<!--more-->

1. **纯静态**  
 相比起Wordpress之类的动态博客，静态博客的主要特点就是不需要数据库，而且通常情况下速度更快

2. **使用Node.js**  
 最流行的静态博客Octopress使用ruby，文章多了以后生成时间会长达几分钟，而Hexo使用了Node.js引擎，速度非常快

3. **支持Markdown**  
 Markdown是一种轻量级标记语言，比HTML更简单，如果你想了解更多，可参考 [维基百科的说明](http://zh.wikipedia.org/wiki/Markdown) 

4. **高拓展性**  
 Hexo现已支持大量插件，其中不少是从Octopress移植而来，具体参见 [官方插件页面](https://github.com/tommy351/hexo/wiki/Plugins) 

5. **快速部署**  
 官方支持GitHub、Heroku和Rsync，只需一道命令即可快速部署。最新加入了通用的git部署方式，可同步到多个git仓库，具体参见 [#452](https://github.com/tommy351/hexo/pull/452) 

总之，Hexo是我认为最棒的静态博客框架！～

---

**2014.01.28 补充：**  
Hexo最大的缺点就是不能随时随地编辑文章，需要在每台电脑部署环境，手机和平板更是不可能完成编辑。好在有 [Koding](https://koding.com/) 这个在线编辑平台，它同时提供了浏览器在线编辑器和支持SSH的虚拟主机。具体可参考 [Joseph](http://www.hahack.com/) 的 [珠联璧合：利用 Koding 为静态博客搭建在线编辑环境](http://www.hahack.com/tools/koding_intro/) 
