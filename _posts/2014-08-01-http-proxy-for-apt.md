title: 修改LMDE的镜像地址，并为apt启用http代理
date: 2014-08-01 17:06:26
tags:
 - linux
 - apt
 - lmde
categories: notes
description: 修改LMDE的仓库镜像，并为包管理apt使用http代理，以提高下载速度
---

使用Linux Mint Debian Edition (LMDE) 有一段时间了，总体感觉很棒，非常适合我的使用习惯。

LMDE的包管理与Debian一样使用apt，不过repo分为两个部分：一部分是`Main`，包含Linux Mint的额外组件；另一部分是`Base`，包含Debian Testing官方库的所有包，不过不同步。

##修改LMDE的repo镜像
LMDE提供了`Software Sources`这个工具来选择repo。也可以在`/etc/apt/sources.list.d/official-package-repositories.list`中修改，使用列表中没有的镜像。

`Main`基本上国内大多数镜像站都会有（一般名为`linuxmint`），例如：
```
deb http://ftp.sjtu.edu.cn/linuxmint/ debian main upstream import
```
不过`Base`一般就没有了，只能使用官方库`debian.linuxmint.com/latest`，保持原样不用修改。

##为apt启用http代理
校园网连接官方库速度很慢，因此考虑使用学校提供的缓存服务`cache.sjtu.edu.cn`作为http代理以加速。

搜索到为apt添加全局http代理的方法：在`/etc/apt/apt.conf`中添加下面一行
```
Acquire::http::Proxy "http://cache.sjtu.edu.cn:8080";
```

但是由于`Main`库已经使用了校内的镜像，无需再使用代理，因此这里只需要为LMDE官方库设置代理即可
```
Acquire::http::Proxy::debian.linuxmint.com "http://cache.sjtu.edu.cn:8080";
```
