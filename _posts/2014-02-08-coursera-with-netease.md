title: 为Coursera强制加载网易视频
date: 2014-02-08 18:01:22
tags:
 - coursera
 - javascript
categories: code
description: 利用用户脚本为所有Coursera课程强制加载网易服务器上的视频文件
---

今天写的一个超级简单的用户脚本，项目主页在GitHub上面：[coursera-netease-userscript](http://github.com/pidanzhou/coursera-netease-userscript)

>Coursera的视频默认使用cloudfront服务器，在国内速度不佳。网易提供了国内服务器，但很多课程都不能自动切换到网易的服务器。这个脚本为所有Coursera课程强制加载网易服务器上的视频文件，暂无检测机制，若网易服务器上没有某门课程的视频文件请自行添加排除规则。

本人之前从未写过userscript，写这个东西完全是个人需求，如有任何错误欢迎指正，谢谢！
