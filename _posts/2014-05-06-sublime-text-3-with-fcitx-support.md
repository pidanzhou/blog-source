title: 使Sublime Text 3支持fcitx输入法
date: 2014-05-06 00:00:15
tags:
 - linux
 - sublime
 - fcitx
categories: code
description: 修复Sublime Text 3中无法输入中文的问题
---

[Sublime Text 3] 一直是我最喜欢的文本编辑器，可惜一直不支持中文输入。之前一直使用一个叫 [InputHelper] 的插件作为临时解决方案，但不知为何现在使用Debian无法再使用这个插件。重新上网搜索之后找到了一个相对完善的方案（[来源]），通过一个补丁使ST3支持fcitx输入法。

[Sublime Text 3]:http://www.sublimetext.com/3
[InputHelper]:https://sublime.wbond.net/packages/InputHelper
[来源]:http://www.sublimetext.com/forum/viewtopic.php?f=3&t=7006&start=10#p41343

##编译补丁
首先需要配置好编译补丁需要的环境：

	sudo apt-get install pkg-config
	sudo apt-get install build-essential
	sudo apt-get install libgtk2.0-dev

<!--more-->然后将以下代码保存为`sublime_imfix.c`
{% gist 6b337ee466a76e729e95 sublime_imfix.c %}

使用以下命令将上述文件编译为so库

	gcc -shared -o libsublime-imfix.so sublime_imfix.c  `pkg-config --libs --cflags gtk+-2.0` -fPIC

此时直接使用以下命令即可启动支持fcitx的Sublime Text

	LD_PRELOAD=./libsublime-imfix.so subl

##让文件管理器和命令行直接调用修复过的ST3
将编译好的`libsublime-imfix.so`复制到`/opt/sublime_text/`

	sudo cp libsublime-imfix.so /opt/sublime_text/

将以下保存为`/usr/share/applications/sublime_text.desktop`
{% gist 6b337ee466a76e729e95 sublime_text.desktop %}
将以下保存为`/usr/bin/subl`
{% gist 6b337ee466a76e729e95 subl %}
